var current_count_files = 0;

jQuery(document).ready(function(j){

    if(window.FileReader == null || !('draggable' in document.createElement('span'))){
        jQuery('#simple_upload').show();
        jQuery('#ajax_upload').remove();
    }

    var body = j('body');
    var loader = j('.loader');
    var max_files = 3;
    var upload_url = 'upload.php';
    var images_list = $('#images-list');
    var drop_zone = j('#drop_zone'),
        max_file_size = (1024 * 1024) * 20, // 20 мегабайт
        allow_types = ['image/jpeg', 'image/png'];

    /**
     * Валидация файла
     *
     * @param file
     * @returns {boolean}
     */
    function validate(file){
        var result = false;

        if(/[а-я]/i.test(file.name)){
            return false;
        }

        allow_types.some(function(type){
            if(file.type == type){
                result = true;
                return true;
            }

            return false;
        });

        if(file.size > max_file_size){
            result = false;
        }

        return result;
    }

    /**
     * Сгенерировать превью картинки
     *
     * @returns {string}
     */
    function setImage(){
        return '<li><a href="#" class="link-delete-preview">X</a><img src="images/loader_white.gif" width="150" /><div class="load_progress"></div></li>'
    }

    /**
     * Создать и добавить превью загружаемой картинки
     *
     * @param file
     */
    function addPreview(file){
        var reader = new FileReader();
        var preview_block = $(setImage()).appendTo(images_list);
        var preview = preview_block.find('img');
        preview.get(0).file = file;

        reader.onload = (function(img){
            return function(e){
                img.attr('src', e.target.result);
                img.attr('title', file.name);
                img.attr('data-id', file.size + file.name);
            };
        })(preview);

        reader.readAsDataURL(file);
    }

    /**
     * Загрузка одного или нескольких файлов на сервер
     *
     * @param files
     * @param image
     */
    function uploadFile(files, image){
        var is_multi_upload = files.length !== undefined;
        var xhr = new XMLHttpRequest();

        if(!is_multi_upload){
            // Выводить прогресс загружаемого файта, если он один
            xhr.upload.addEventListener('progress', function(e){
                var percent = parseInt(e.loaded / e.total * 100);
                image.parent().find('.load_progress').text(percent + '%');
            }, false);
        }

        // Формирование и отправка запроса на сервер
        xhr.open('POST', upload_url);
        var data = new FormData();

        if(is_multi_upload){
            for(var i = 0; i < files.length; i++){
                data.append('image[]', files[i]);
            }
        }else{
            data.append('images', files);
        }

        xhr.send(data);
    }

    // События наведения на область загрузки
    drop_zone[0].ondragover = function(){
        drop_zone.addClass('hover');
        return false;
    };
    drop_zone[0].ondragleave = function(){
        drop_zone.removeClass('hover');
        return false;
    };

    // Событие загрузки
    drop_zone[0].ondrop = function(event){
        event.preventDefault();
        drop_zone.removeClass('hover');
        if(current_count_files >= max_files){
            drop_zone.find('span').text('Можно загружать не больше ' + max_files + ' файлов');
            return;
        }

        var files = event.dataTransfer.files;
        for(var i = 0; i <= (max_files - current_count_files); i++){
            if(!validate(files[i])){
                continue;
            }

            current_count_files++;
            addPreview(files[i]);
        }

        return true;
    };

    body.on('click', '#send', function(){
        event.preventDefault();

        drop_zone.find('span').remove();
        loader.show();

        var files = [];
        j.each($('#images-list li img'), function(i, img){
            files.push(img.file);
        });

        uploadFile(files);
        drop_zone.text('Загрузка завершена.');
    });

    body.on('click', '.link-delete-preview', function(){
        event.preventDefault();
        $(this).parents('li').remove();
        current_count_files--;
    });

});