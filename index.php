<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ajax file upload</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="scripts.js"></script>
    <link rel="stylesheet" href="style.css" />
</head>
<body>

<div id="ajax_upload">
    <div id="drop_zone">
        <span>Для загрузки, перетащите файлы сюда.</span>
        <img src="images/loader.gif" alt="" class="loader" />
    </div>

    <a href="" id="send">Отправить</a><br />

    <ul id="images-list"></ul>
</div>

<div id="simple_upload" style="display: none;">asd</div>

</body>
</html>